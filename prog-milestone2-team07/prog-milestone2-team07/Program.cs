﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team07
{
    class Program
    {
        //The main menu
        static void Menu()
        {
            
            Console.Clear();

            Console.WriteLine("\n                   Main    Menu");
            Console.WriteLine("****************************************************\n");
            Console.WriteLine("             1. Date Calculator\n");
            Console.WriteLine("             2. Average Grade Calculator\n");
            Console.WriteLine("             3. Random Number Generator\n");
            Console.WriteLine("             4. Favorite Food Rating\n");
            Console.WriteLine("             Press 'E' to CLOSE Console!\n");
            Console.WriteLine("****************************************************\n");
            Console.WriteLine("             Please Select An Option");

        }


        //Return to the main menu OR Close the console
        static void CloseOrReturn()
        {
            bool InValid = true;
            do
            {
                Console.WriteLine("\nPress 'M' to Return to the Main Menu.\nPress 'E' to Close the Console.\n");
                var input = Console.ReadLine().ToLower();

                if (input == "m")
                {
                    Menu();
                    InValid = false;
                }
                else if (input == "e")
                {
                    Environment.Exit(0);
                    InValid = false;
                }
                if(InValid)
                {
                    Console.Clear();
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
            } while (InValid);
            
        }


        //Task 1 - Date Calculator
        static void DateCalculator()
        {
            while (true)
            {
                DateTime today = DateTime.Today;
                Console.Write("\nPlease enter your date of birth (dd/mm/yyyy): ");
                var input = Console.ReadLine();
                DateTime date;
                bool IsValid = DateTime.TryParse(input, out date);

                if (!IsValid)
                {
                    Console.WriteLine("Invalid Date Input! Please Try Again!\n");
                    input = string.Empty;
                }
                else
                {
                    TimeSpan difference = today - date;
                    int days = difference.Days;
                    Console.WriteLine("\nIt has been '" + days + "' days since the day you were born.");
                    break;
                }
            }
        }


        //Task 2 - Calculate Average Grade
        //static void AverageGrade()
        //{
        //    //Please paste the methods here!
        //}


        //Task 3 - Generate a Random Number
        //static void RandomNumber()
        //{
        //    //Please paste the methods here! 
        //}


        //Task 4 - Rate Favourite Food
        //static void FavouriteFood()
        //{
        //    //Please paste the methods here! 
        //}


        static void Main(string[] args)
        {           
            while(true)
            {
                Menu();

                var userinput = Console.ReadLine().ToLower();
                int i;
                bool IsValid = int.TryParse(userinput, out i);
                bool IsEmpty = string.IsNullOrWhiteSpace(userinput);

                if (userinput == "e")
                {
                    Environment.Exit(0);
                    break;
                }

                if (!IsValid || i > 4 || IsEmpty)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid Input! Please Enter a Valid Number!");
                    Console.ReadKey();
                    userinput = string.Empty;
                }

                else
                {
                    switch (i)
                    {
                        case 1:
                            Console.Clear();

                            DateCalculator();

                            CloseOrReturn();
                            break;

                        case 2:
                            Console.Clear();

                            //AverageGrade();

                            CloseOrReturn();
                            break;

                        case 3:
                            Console.Clear();

                            //RandomNumber();

                            CloseOrReturn();
                            break;

                        case 4:
                            Console.Clear();

                            //FavouriteFood();

                            CloseOrReturn();
                            break;
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
