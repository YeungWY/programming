﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                DateTime today = DateTime.Today;
                Console.Write("Please enter your date of birth (dd/mm/yyyy): ");
                var input = Console.ReadLine();
                DateTime date;
                bool IsValid = DateTime.TryParse(input, out date);

                if (!IsValid)
                {
                    Console.WriteLine("Invalid Date Input! Please Try Again!\n");
                    input = string.Empty;
                }
                else
                {
                    TimeSpan difference = today - date;
                    int days = difference.Days;
                    Console.WriteLine("\nIt has been '" + days + "' days since the day you were born.");
                    break;
                }
            }            
            Console.ReadLine();
        }
    }
}
